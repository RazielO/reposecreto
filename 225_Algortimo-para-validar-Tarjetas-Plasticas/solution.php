<?php
function algorithm($card)
{
    $result = 0;
    $parity = strlen($card) % 2;

    for ($i = 0; $i < strlen($card); $i++) {
        $partial = (int)$card[$i];

        if ($i % 2 == $parity) {
            $partial *= 2;
            if ($partial > 9)
                $partial = ($partial % 10) + (int)($partial / 10);
        }

        $result += $partial;    
    }

    return $result;
}

fscanf(STDIN, "%i", $n);

while ($card = trim(fgets(STDIN))) {

    $val = algorithm($card);
    $result = ($val % 10) === 0 ? 'VALIDA' : 'NO Valida';
    fwrite(STDOUT, ("$result" . PHP_EOL));
}
