<?php
function solve($x) {
	$result = 1;

	for ($i = 1; $i <= $x; $i++)
		$result += ($i * 4) - 4;

	return $result;
}

fscanf(STDIN, "%i", $n);

for ($i = 0 ; $i < $n; $i++) {
	fscanf(STDIN, "%i", $x);

	fwrite(STDOUT, (solve($x) . PHP_EOL));
}