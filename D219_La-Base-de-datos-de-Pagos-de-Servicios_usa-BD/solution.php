<?php
fscanf(STDIN, "%s", $server);
fscanf(STDIN, "%s", $user);
fscanf(STDIN, "%s", $password);
fscanf(STDIN, "%s", $db);

$conn = mysqli_connect($server, $user, $password, $db);
$tableName = "BD_PagoServ_Facturas";

$foreignQuery = "SELECT k.COLUMN_NAME            AS ForeignField,
                        k.REFERENCED_TABLE_NAME  AS ForeignTable,
                        k.REFERENCED_COLUMN_NAME AS PrimaryField,
                        c.COLUMN_TYPE            AS ColumnType
                     FROM information_schema.TABLE_CONSTRAINTS i
                              LEFT JOIN information_schema.KEY_COLUMN_USAGE k ON i.CONSTRAINT_NAME = k.CONSTRAINT_NAME
                              JOIN information_schema.COLUMNS c ON k.COLUMN_NAME = c.COLUMN_NAME
                     WHERE i.CONSTRAINT_TYPE = 'FOREIGN KEY'
                       AND i.TABLE_SCHEMA = '$db'
                       AND i.TABLE_NAME = '$tableName'
                       ORDER BY ForeignField;";

$primaryQuery = "SELECT kcu.COLUMN_NAME, c.COLUMN_TYPE
    FROM information_schema.table_constraints tco
             JOIN information_schema.key_column_usage kcu
                  ON tco.constraint_schema = kcu.constraint_schema
                      AND tco.constraint_name = kcu.constraint_name
                      AND tco.table_name = kcu.table_name
             JOIN information_schema.COLUMNS c
                  ON kcu.COLUMN_NAME = c.COLUMN_NAME
                      AND kcu.TABLE_SCHEMA = c.TABLE_SCHEMA
                      AND kcu.TABLE_NAME = c.TABLE_NAME
    WHERE tco.constraint_type = 'PRIMARY KEY'
      AND tco.table_schema = '$db'
      AND tco.TABLE_NAME = '$tableName'
    ORDER BY tco.table_schema,
             tco.table_name,
             kcu.ordinal_position;";

if ($conn) {
    $foreignData = mysqli_fetch_all(mysqli_query($conn, $foreignQuery), MYSQLI_ASSOC);
    $primaryData = mysqli_fetch_assoc(mysqli_query($conn, $primaryQuery));

    fwrite(STDOUT, "Nombre de llave primaria: " . $primaryData['COLUMN_NAME'] . " [" . $primaryData['COLUMN_TYPE'] . "]" . PHP_EOL);
    fwrite(STDOUT, "Foraneas:" . PHP_EOL);

    foreach ($foreignData as $table)
        fwrite(STDOUT, "Nombre:" . $table['ForeignField'] .  " <=> Tabla Referenciada:" . $table['ForeignTable'] . " <=> CampoForaneo:" . $table['PrimaryField'] . " <=> [" . $table['ColumnType'] . "]" . PHP_EOL);
}
