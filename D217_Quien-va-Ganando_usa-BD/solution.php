<?php
fscanf(STDIN, "%s", $server);
fscanf(STDIN, "%s", $user);
fscanf(STDIN, "%s", $pass);
fscanf(STDIN, "%s", $db);

$conn = mysqli_connect($server, $user, $pass, $db);

if ($conn) {
    $pointsQuery = "SELECT ganador, SUM(puntos) AS suma
                        FROM BD_Domino_Juegos 
                        GROUP BY ganador
                        HAVING suma > 0
                        ORDER BY suma DESC
                        LIMIT 1;";

    $points = mysqli_fetch_assoc(mysqli_query($conn, $pointsQuery));

    $userQuery = "SELECT Nombre, Apellidos
                    FROM Usuarios 
                    WHERE Usuario = '" . $points['ganador'] . "';";

    $data = mysqli_fetch_assoc(mysqli_query($conn, $userQuery));

    $result = $data['Nombre'] . " " . $data['Apellidos'] . " " . $points['suma'] . PHP_EOL;

    fwrite(STDOUT, $result);
}