<?php

fscanf(STDIN, "%s", $server);
fscanf(STDIN, "%s", $user);
fscanf(STDIN, "%s", $pass);
fscanf(STDIN, "%s", $db);

$conn = mysqli_connect($server, $user, $pass, $db);

if ($conn) {
    $query = "SELECT DISTINCT tco.TABLE_NAME
            FROM information_schema.table_constraints tco
            WHERE tco.table_schema = '$db'
            ORDER BY tco.table_name DESC;";

    $tables = mysqli_fetch_all(mysqli_query($conn, $query), MYSQLI_NUM);

    $result = "";
    foreach ($tables as $table)
        $result .= "$table[0]:";

    fwrite(STDOUT, substr($result, 0, strlen($result) - 1) . PHP_EOL);
}